//
//  RegisterInteractor.swift
//  Formulario
//
//  Created by Camilo Soto on 20/12/19.
//  Copyright © 2019 csoto. All rights reserved.
//

import Foundation


class RegisterInteractor: RegisterContractInteractor {
    
    var callback: RegisterContractCallback?
    
    //MARK: Interactor
    func setCallback(callback: RegisterContractCallback) {
        self.callback = callback
    }
    
    func getListCities() {
        let cities = ["Bogotá", "Medellín", "Cali", "Cartagena", "Barranquilla", "Tunja"]
        callback?.returnListCities(listcities: cities)
    }
    
    func saveRegister(name: String, date: String, address: String, placa: String, city: String) {
        
    }
    
    
}
