//
//  ViewController.swift
//  Formulario
//
//  Created by Camilo Soto on 20/12/19.
//  Copyright © 2019 csoto. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    @IBOutlet weak var img_logo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animation()
    }
    
    func animation(){
        UIView.animate(withDuration: 2.0, animations: {() -> Void in
            self.img_logo.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 1.0, animations: {() -> Void in
                self.img_logo.transform = CGAffineTransform(scaleX: 1, y: 1)
                let mainQueue = DispatchQueue.main
                let deadline = DispatchTime.now() + .seconds(3)
                mainQueue.asyncAfter(deadline: deadline) {
                    self.goNext()
                }
            })
        })
    }

    func goNext(){
        performSegue(withIdentifier: "toRegister", sender: self)
    }
}

