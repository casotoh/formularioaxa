//
//  RegisterVC.swift
//  Formulario
//
//  Created by Camilo Soto on 20/12/19.
//  Copyright © 2019 csoto. All rights reserved.
//

import UIKit
import MapKit

class RegisterVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, RegisterContactView {

    @IBOutlet weak var img_photo: UIImageView!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldPlaca: UITextField!
    @IBOutlet weak var txtFieldDate: UITextField!
    @IBOutlet weak var txtFieldAdress: UITextField!
    @IBOutlet weak var txtFieldCity: UITextField!
    @IBOutlet weak var bntSave: UIButton!
    @IBOutlet weak var btnCloseMap: UIButton!
    @IBOutlet weak var btnSelectAdress: UIButton!
    
    @IBOutlet weak var mapView: MKMapView!
    
    var presenter: RegisterContractPresenter?
    var listCities: [String] = []
    var citySelect = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.presenter = RegisterPresenter(view: self, interactor: RegisterInteractor())
        txtFieldDate.addTarget(self, action: #selector(openCalendar), for: .editingDidBegin)
        txtFieldAdress.addTarget(self, action: #selector(openMap), for: .editingDidBegin)
        txtFieldCity.addTarget(self, action: #selector(showListCities), for: .editingDidBegin)
    }
    
    //MARK: View
    func chargeListcities(listCities: [String]) {
        self.listCities = listCities
    }
    
    func goResume() {
        
    }
    
    func showMessage(message: String) {
        let alert = UIAlertController(
            title: "Alerta",
            message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func selectPhoto(_ sender: UIButton) {
    }
    
    
    @IBAction func save(_ sender: UIButton) {
    
        let name = txtFieldName.text!
        let date = txtFieldDate.text!
        let address = txtFieldAdress.text!
        let placa = txtFieldPlaca.text!
        let city = txtFieldCity.text!
        
        presenter?.saveInfo(name: name, date: date, address: address, placa: placa, city: city)
    }
    
    @IBAction func closeMap(_ sender: UIButton) {
        self.btnCloseMap.isHidden = true
        self.btnSelectAdress.isHidden = true
        self.mapView.isHidden = true
    }
    
    @IBAction func selectAdress(_ sender: UIButton) {
        self.btnCloseMap.isHidden = true
        self.btnSelectAdress.isHidden = true
        self.mapView.isHidden = true
    }
    
    @objc func openCalendar() {
        let pickerDate: UIDatePicker = UIDatePicker()
        pickerDate.datePickerMode = .date
        pickerDate.timeZone = NSTimeZone.local
        pickerDate.frame = CGRect(x: 0, y: 15, width: 270, height: 200)
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.alert)
        alertController.view.addSubview(pickerDate)
        let selectAction = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: { _ in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let dateStr = dateFormatter.string(from: pickerDate.date)
            self.txtFieldDate.text = dateStr
        })
        let cancelAction = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.cancel, handler: nil)
        alertController.addAction(selectAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion:{})
    }
    
    @objc func openMap(){
        self.btnCloseMap.isHidden = false
        self.btnSelectAdress.isHidden = false
        self.mapView.isHidden = false
    }
    
    // Funcion para visualizar la lista de ciudades
    @objc func showListCities() {
        self.citySelect = listCities[0]
        
        let alertView = UIAlertController(
            title: "Seleccione una ciudad",
            message: "\n\n\n\n\n\n\n\n\n",
            preferredStyle: .alert)
        
        let pickerView = UIPickerView(frame:
            CGRect(x: 0, y: 50, width: 250, height: 160))
        pickerView.dataSource = self
        pickerView.delegate = self
        
        // comment this line to use white color
        pickerView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
        
        alertView.view.addSubview(pickerView)
        
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: {action in
            self.txtFieldCity.text = self.citySelect
        })
        
        alertView.addAction(action)
        present(alertView, animated: true, completion: {
            pickerView.frame.size.width = alertView.view.frame.size.width
        })
    }
    
    
    // Delegate Picker View
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.listCities.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.listCities[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.citySelect = self.listCities[row]
    }
    
}
