//
//  RegisterContract.swift
//  Formulario
//
//  Created by Camilo Soto on 20/12/19.
//  Copyright © 2019 csoto. All rights reserved.
//

import Foundation


protocol RegisterContactView {
    func chargeListcities(listCities: [String])
    func goResume()
    func showMessage(message: String)
}

protocol RegisterContractPresenter {
    func saveInfo(name: String, date: String, address: String, placa: String, city: String)
}

protocol RegisterContractInteractor {
    func setCallback(callback: RegisterContractCallback)
    func getListCities()
    func saveRegister(name: String, date: String, address: String, placa: String, city: String)
}

protocol RegisterContractCallback {
    func returnListCities(listcities: [String])
    func saveSuccess()
    func saveError(message: String)
}
