//
//  RegisterPresenter.swift
//  Formulario
//
//  Created by Camilo Soto on 20/12/19.
//  Copyright © 2019 csoto. All rights reserved.
//

import Foundation


class RegisterPresenter: RegisterContractPresenter, RegisterContractCallback {
    
    var view: RegisterContactView?
    var interactor: RegisterContractInteractor?
    
    
    init(view: RegisterContactView, interactor: RegisterContractInteractor) {
        self.view = view
        self.interactor = interactor
        self.interactor?.setCallback(callback: self)
        self.interactor?.getListCities()
    }
    
    //MARK: Presenter
    func saveInfo(name: String, date: String, address: String, placa: String, city: String) {
        self.interactor?.saveRegister(name: name, date: date, address: address, placa: placa, city: city)
    }
    
    
    //MARK: Callback
    func returnListCities(listcities: [String]) {
        self.view?.chargeListcities(listCities: listcities)
    }
    
    func saveSuccess() {
        self.view?.goResume()
    }
    
    func saveError(message: String) {
        self.view?.showMessage(message: message)
    }
}
